/*
 * button.h
 *
 *  Created on: 06.03.2020
 *      Author: jonas
 */

#ifndef SOURCES_BUTTON_H_
#define SOURCES_BUTTON_H_


void waitForButton(void);

void buttonInit(void);


#endif /* SOURCES_BUTTON_H_ */
