/*
 * drive.c
 *
 *  Created on: 21.02.2020
 *      Author: Marco Anderhalden, Riccardo Degiacomi, Jonas Tophinke
 */


#include "platform.h"
#include "drive.h"
#include "ftm3.h"


//direction of the motors
#define MOTOR_RIGHT_F()			  (GPIOC_PDOR |= 1 << 9)
#define MOTOR_RIGHT_B()			  (GPIOC_PDOR &= ~(1<<9))

#define MOTOR_LEFT_F()			  (GPIOC_PDOR |= 1 << 11)
#define MOTOR_LEFT_B()		  	  (GPIOC_PDOR &= ~(1<<11))


static int8_t valueRight;
static int8_t valueLeft;

void motorIncrementPwmRight(int8_t value) {
  int32_t v = valueRight + value;
  if (v > MOTOR_MAX_VALUE)
    v = MOTOR_MAX_VALUE;
  if (v < -MOTOR_MAX_VALUE)
    v = -MOTOR_MAX_VALUE;
  motorSetPwmRight((int8_t) v);
}
;

void motorIncrementPwmLeft(int8_t value) {
  int32_t v = valueLeft + value;
  if (v > MOTOR_MAX_VALUE)
    v = MOTOR_MAX_VALUE;
  if (v < -MOTOR_MAX_VALUE)
    v = -MOTOR_MAX_VALUE;
  motorSetPwmLeft((int8_t) v);
}
;

void motorSetPwmRight(int8_t value) {
  if (value > MOTOR_MAX_VALUE)
    value = MOTOR_MAX_VALUE;
  if (value < -MOTOR_MAX_VALUE)
    value = -MOTOR_MAX_VALUE;
  valueRight = value;

  if (value < 0) {
    // drive backward
    value = -value;             // value has to be a positive channel value!
    MOTOR_RIGHT_B();       		// sets motor direction to backward
  } else if (value > 0) {
    // drive forward
	MOTOR_RIGHT_F();       		// sets motor direction to forward
  } else {
    // stop
    value = 0;

  }
  int16_t v = (uint16_t) (((FTM3_MODULO + 1) * ((uint32_t) value))
      / MOTOR_MAX_VALUE);
  FTM3_C4V = v;
}
;

void motorSetPwmLeft(int8_t value) {
  if (value > MOTOR_MAX_VALUE)
    value = MOTOR_MAX_VALUE;
  if (value < -MOTOR_MAX_VALUE)
    value = -MOTOR_MAX_VALUE;
  valueLeft = value;

  if (value < 0) {
      // drive backward
      value = -value;             	// value has to be a positive channel value!
      MOTOR_LEFT_B();       		// sets motor direction to backward
    } else if (value > 0) {
      // drive forward
      MOTOR_LEFT_F();       		// sets motor direction to forward
    } else {
      // stop
      value = 0;

  }
  int16_t v = (uint16_t) (((FTM3_MODULO + 1) * ((uint32_t) value))
      / MOTOR_MAX_VALUE);
  FTM3_C6V = v;

}
;

void motorInit(void) {
  // Configure the pin direction of the 4 pins as output.
  // configures the pin muxing

  (PORTC_PCR8 = PORT_PCR_MUX(3));
  (PORTC_PCR9 = PORT_PCR_MUX(1));
  (PORTC_PCR10 = PORT_PCR_MUX(3));
  (PORTC_PCR11 = PORT_PCR_MUX(1));


  GPIOC_PDDR |= (1 << 9);
  GPIOC_PDOR |= (1 << 9);
  GPIOC_PDDR |= (1 << 11);
  GPIOC_PDOR |= (1 << 11);

  PORTC_PCR9 |= PORT_PCR_PE(0);
  PORTC_PCR11 |= PORT_PCR_PE(0);

  // Set pin value of the pins to '1'
  GPIOC_PDOR |= 1 << 9 | 1 << 11;



  // configure both channels as edge aligned PWM with high-true pulses
  FTM3_C4SC |= FTM_CnSC_ELSA(0) | FTM_CnSC_ELSB(1) | FTM_CnSC_MSB(1);
  FTM3_C6SC |= FTM_CnSC_ELSA(0) | FTM_CnSC_ELSB(1) | FTM_CnSC_MSB(1);


}
;

