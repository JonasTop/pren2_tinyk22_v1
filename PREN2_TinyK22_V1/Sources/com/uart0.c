/**
 *--------------------------------------------------------------------\n
 *          HSLU T&A Hochschule Luzern Technik+Architektur            \n
 *--------------------------------------------------------------------\n
 *
 * \brief         driver for the serial communication
 * \file
 * \author        Christian Jost, christian.jost@hslu.ch
 * \changes		  Marco Anderhalden, Riccardo Degiacomi, Jonas Tophinke
 * \date          19.03.2018
 *
 * $Id: uart0.c 83 2018-04-11 12:20:43Z zajost $
 *
 *--------------------------------------------------------------------
 */
#include "platform.h"
#include "uart.h"

#if UART0_EN


/**
 * the receive queue of this driver, implemented as ring buffer
 */
static uint32_t rxBufInt[UART0_RX_BUF_SIZE];    //buffer for cat. int32 Value received from Raspy
static volatile uint16_t rxBufIntCount;
static uint16_t rxBufIntWritePos;
static uint16_t rxBufIntReadPos;

/**
 * the receive queue of this driver, implemented as ring buffer
 */
static char rxBuf[UART0_RX_BUF_SIZE];
static volatile uint16_t rxBufCount;
static uint16_t rxBufWritePos;
static uint16_t rxBufReadPos;


#define ENABLE_UART0_INTERRUPTS()     NVIC_EnableIRQ(UART0_RX_TX_IRQn)
#define DISABLE_UART0_INTERRUPTS()    NVIC_DisableIRQEx(UART0_RX_TX_IRQn)


/**
 * @brief UART Interrupt Service Routine
 * - Received bytes are stored in the queue rxBuf
 */
void UART0_RX_TX_IRQHandler(void) {
	OnEnterUart0RxTxISR(); 			 // turns Debug-LED on

	uint8_t status = UART0_S1;
	uint8_t data = UART0_D;
	if (status & UART_S1_RDRF_MASK) {
		if (rxBufWritePos < UART0_RX_BUF_SIZE) {
			rxBuf[rxBufWritePos++] = data;
			rxBufCount++;
			if (rxBufWritePos == UART0_RX_BUF_SIZE)
				rxBufWritePos = 0;
		}
		if (rxBufIntWritePos < UART0_RX_BUF_SIZE) {
			if (data == 0xFF) {
				rxBufIntCount++;
				rxBufIntWritePos++;
			} else {

				rxBufInt[rxBufIntWritePos] = (rxBufInt[rxBufIntWritePos] << 8)
						| (data);
			}
			if (rxBufIntWritePos == UART0_RX_BUF_SIZE)
				rxBufIntWritePos = 0;
		}
	}

	OnExitUart0RxTxISR();				// turns Debug-LED off
}

/**
 * Error Interrupt Service Routine
 * Clears the error flags.
 */
void UART0_ERR_IRQHandler(void)
{
  (void)UART0_S1;
  (void)UART0_D;
}



uint32_t uart0ReadInt32(void)
{
 return rxBufInt[rxBufIntWritePos-1];
}


/**
 * Reads one char out of the rxBuf queue. The function blocks
 * until there is at least one byte in the queue.
 *
 * @return
 *   the received byte
 */
char uart0ReadChar(void)
{
  char ch;
  while (rxBufCount == 0);
  ch = rxBuf[rxBufReadPos++];
  if (rxBufReadPos == UART0_RX_BUF_SIZE) rxBufReadPos = 0;
  DISABLE_UART0_INTERRUPTS();
  rxBufCount--;
  ENABLE_UART0_INTERRUPTS();
  return ch;
}

uint8_t uart0ReadNumber(void)
{
  uint8_t n;
  while (rxBufCount == 0);
  n = rxBuf[rxBufReadPos++];
  if (rxBufReadPos == UART0_RX_BUF_SIZE) rxBufReadPos = 0;
  DISABLE_UART0_INTERRUPTS();
  rxBufCount--;
  ENABLE_UART0_INTERRUPTS();
  return n;
}




/**
 * Returns the number of unt32 values in the receiver queue.
 *
 */
uint16_t uart0RxIntBufCount(void)
{
  return rxBufIntCount;
}




/**
 * initializes the uart with the desired baud rate.
 *
 *   tinyK22:
 *   - PTD6 Mux3:UART0_RX
 *   - PTD7 Mux3:UART0_TX
 */
void uart0Init(uint16_t baudrate)
{
  rxBufReadPos = rxBufWritePos = rxBufCount = 0;

  // configure clock gating
  SIM_SCGC4 |= SIM_SCGC4_UART0_MASK;

  // configure port multiplexing, enable Pull-Ups and enable OpenDrain (ODE)!
  // OpenDrain is needed to ensure that no current flows from Target-uC to the Debugger-uC
  PORTD_PCR6 = PORT_PCR_MUX(3) | PORT_PCR_PE(1) | PORT_PCR_PS(1) | PORT_PCR_ODE_MASK;
  PORTD_PCR7 = PORT_PCR_MUX(3) | PORT_PCR_PE(1) | PORT_PCR_PS(1) | PORT_PCR_ODE_MASK;

  // set the baudrate into the BDH and BDL register
  uint32_t bd = (CORECLOCK / (16 * baudrate));
  UART0_BDH = (bd >> 8) & 0x1F;
  UART0_BDL = bd & 0xFF;

  // enable uart receiver, receiver interrupt and transmitter as well as
  // enable and set the rx/tx interrupt in the nested vector interrupt controller (NVIC)
  UART0_C2 = UART_C2_RIE_MASK | UART_C2_RE_MASK | UART_C2_TE_MASK;
  NVIC_SetPriority(UART0_RX_TX_IRQn, PRIO_UART0);
  NVIC_EnableIRQ(UART0_RX_TX_IRQn);

  // enable the error interrupts of the uart and configure the NVIC
  UART0_C3 = UART_C3_ORIE_MASK | UART_C3_NEIE_MASK | UART_C3_FEIE_MASK;
  NVIC_SetPriority(UART0_ERR_IRQn, PRIO_UART0);
  NVIC_EnableIRQ(UART0_ERR_IRQn);
}
#endif
