/*
 * drive.h
 *
 *  Created on: 21.02.2020
 *      Author: Marco Anderhalden, Riccardo Degiacomi, Jonas Tophinke
 */
#include "platform.h"
#include "stdint.h"


#ifndef SOURCES_PWM_MOTOR_H_
#define SOURCES_PWM_MOTOR_H_

#define MOTOR_MAX_VALUE               127

void motorIncrementPwmRight(int8_t value);
void motorIncrementPwmLeft(int8_t value);

void motorSetPwmRight(int8_t value);
void motorSetPwmLeft(int8_t value);

void motorInit(void);


#endif  /* SOURCES_PWM_MOTOR_H_*/
