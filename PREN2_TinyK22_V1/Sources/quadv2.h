/*
 * quadv2.h
 *
 *  Created on: 13.03.2020
 *      Author: Author: Marco Anderhalden, Riccardo Degiacomi, Jonas Tophinke
 */

#include "unistd.h"

#ifndef SOURCES_QUADV2_H_
#define SOURCES_QUADV2_H_


int32_t quadv2GetSpeedLeft(void);

int32_t quadv2GetSpeedRight(void);


void quadv2Init(void);


#endif /* SOURCES_QUADV2_H_ */
