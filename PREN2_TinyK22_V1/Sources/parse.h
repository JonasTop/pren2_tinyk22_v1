/*Parse.c
 * This file is meant to take the Hex Val. Sent from the raspberry pi and
 * decompose it. It contains functions to parse the Hex val int 4 different values.
 * 1Angle value and 2 speed values. The angle value will be translated into a speed
 * difference.
 * The output of these functions will be used in drive.c
 *
 *
 *  Created on: 21.02.2020
 *      Author: Marco Anderhalden, Riccardo Degiacomi, Jonas Tophinke
 */
//returns the speedvalue sent by the py
uint16_t getspeed(uint32_t num_in);
//returns the angle value sent by the py
uint16_t getangle(uint32_t num_in);
//translate value into speed
uint32_t toSpeed(uint32_t speed);
//returns calculated speed in mm/sek
uint8_t getSpeedLeft(void);
//returns calculated speed in mm/sek
uint8_t getSpeedRight(void);
//translate value into speeddifference (equals direction)
uint8_t toDirection(uint8_t direction);

//gets value from raspberry via uart
uint8_t getNum(uint8_t num_in);
