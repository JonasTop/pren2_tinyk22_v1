/*
 * quadv2.c
 *
 *  Created on: 13.03.2020
 *      Author: Marco Anderhalden, Riccardo Degiacomi, Jonas Tophinke
 */

#include <string.h>
#include "platform.h"
#include "quad.h"
#include "term.h"
#include "quadv2.h"

#define FTM_CLOCK                 250000    // 250 kHz
#define FTM_PRESCALE              1         // div 1
#define WHEEL_DIAMETER            110      	// 110 mm
#define TICKS_PER_REVOLUTION      512       // 512 ticks per wheel revolution

#define WHEEL_CIRCUMFERENCE       (WHEEL_DIAMETER * 3.141593)                                       // ?? mm
//#define UM_PER_TICK               (((10000.0 * WHEEL_CIRCUMFERENCE / TICKS_PER_REVOLUTION)+5)/10)   // ?? => ?? Micrometer/Ticks

static uint32_t revolutionsPerMinuteLeft = 0;
static uint32_t ticksLeft = 0;

static uint32_t revolutionsPerMinuteRight = 0;
static uint32_t ticksRight = 0;


int32_t quadv2GetSpeedLeft(void) {
	return (revolutionsPerMinuteLeft*WHEEL_CIRCUMFERENCE)/60;
}

int32_t quadv2GetSpeedRight(void) {
	return (revolutionsPerMinuteRight*WHEEL_CIRCUMFERENCE)/60;
}


void FTM0_IRQHandler(void) {
	OnEnterQuadLeftISR();							// turns debug led on

	if (FTM0_SC & FTM_SC_TOF_MASK) {
		FTM0_SC &= ~FTM_SC_TOF_MASK;				// clear timer overflow flag
	}

	revolutionsPerMinuteLeft = ((ticksLeft * 60)/(32 * 10));
	ticksLeft = 0;

	revolutionsPerMinuteRight = ((ticksRight * 60)/(32 * 10));
	ticksRight = 0;

	OnExitQuadLeftISR();							// turns debug led off
}

void FTM1_IRQHandler(void) {
	OnEnterQuadLeftISR();							// turns debug led on

	if (FTM1_SC & FTM_SC_TOF_MASK) {
		FTM1_SC &= ~FTM_SC_TOF_MASK;				// clear timer overflow flag
	}

	ticksLeft ++;

	OnExitQuadLeftISR();							// turns debug led off
}

void FTM2_IRQHandler(void) {
	OnEnterQuadLeftISR();							// turns debug led on

	if (FTM2_SC & FTM_SC_TOF_MASK) {
		FTM2_SC &= ~FTM_SC_TOF_MASK;				// clear timer overflow flag
	}

	ticksRight ++;

	OnExitQuadLeftISR();							// turns debug led off
}

void quadv2Init(void) {
	PORTB_PCR18 = PORT_PCR_MUX(6);
	PORTB_PCR19 = PORT_PCR_MUX(6);
	PORTA_PCR12 = PORT_PCR_MUX(7);
	PORTA_PCR13 = PORT_PCR_MUX(7);

	// set clockgating for FTM1 & FTM2
	SIM_SCGC6 |= SIM_SCGC6_FTM1_MASK | SIM_SCGC6_FTM2_MASK;

	// configure the timer with "Fixed frequency clock" as clocksource and with a "Prescaler" of 0 => 250'000 kHz
	FTM1_SC =  FTM_SC_TOIE(1);
	FTM2_SC =  FTM_SC_TOIE(1);

	//interrupt for each revolution (512 * 4 = all edges)/32 = 1/32 revolution ( quad-mode counts every edge from A & B!)
	FTM1_MOD = 0x40;
	FTM2_MOD = 0x40;

	//sets the initial counter value to zero
	FTM1_CNTIN=0x00;
	FTM2_CNTIN=0x00;

	FTM1_C0SC = 0x00;		//quad mode
	FTM1_C1SC = 0x00;

	FTM2_C0SC = 0x00;		//quad mode
	FTM2_C1SC = 0x00;


	FTM1_MODE|=0x05; //quadrature mode which PhaseA and PhaseB signal have 90 degree shift (write protection is disabled)

	// configure the quad mode
	FTM1_QDCTRL |= FTM_QDCTRL_PHAFLTREN_MASK | FTM_QDCTRL_PHBFLTREN_MASK | FTM_QDCTRL_QUADEN_MASK;
	FTM2_QDCTRL |= FTM_QDCTRL_PHAFLTREN_MASK | FTM_QDCTRL_PHBFLTREN_MASK | FTM_QDCTRL_QUADEN_MASK;

	// Enable FTM1+2 interrupt on NVIC with Prio: PRIO_FTM0 (defined in platform.h)
	NVIC_SetPriority(FTM1_IRQn, PRIO_FTM1);       	// set interrupt priority
	NVIC_EnableIRQ(FTM1_IRQn);                     	// enable interrupt

	NVIC_SetPriority(FTM2_IRQn, PRIO_FTM2);        	// set interrupt priority
	NVIC_EnableIRQ(FTM2_IRQn);                     	// enable interrupt



	// clockgating for FTM0
	SIM_SCGC6 |= SIM_SCGC6_FTM0(1);

	// sets the modulo (in this case -> 100ms)
	FTM0_MOD = 0x51A7;  //20903


	// configures the timer with "Fixed frequency clock" as clocksource and with a "Prescaler" of 2 => 250'000 kHz
	FTM0_SC |= FTM_SC_CLKS(2) | FTM_SC_PS(0) | FTM_SC_TOIE_MASK;

	// enables FTM0 interrupt on NVIC with Prio: PRIO_FTM0 (defined in platform.h)
	NVIC_EnableIRQ(FTM0_IRQn);
	NVIC_SetPriority(FTM0_IRQn, PRIO_FTM0);

}

