/*
 * main.c
 *
 *  Created on: 21.02.2020
 *      Author: Marco Anderhalden, Riccardo Degiacomi, Jonas Tophinke
 */

#include "drive.h"
#include "platform.h"
#include "stdint.h"
#include "unistd.h"
#include "stdlib.h"
#include "ftm3.h"
#include "parse.h"
#include "button.h"
#include "quadv2.h"
#include "uart.h"
#include "stdio.h"

void main(void) {

	EnableDebugLeds();
	quadv2Init();
	ftm3Init();
	motorInit();
	uart0Init(9600);
	uart1Init(56000);

//  motor test:
//
//	for (int i= 0; i<127; i++){
//		motorIncrementPwmLeft(1);
//
//		for (int x = 0; x < 400000; x++) {
//					__asm("NOP");
//				}
//
//	}
//
//	for (int x = 0; x < 40000000; x++) {
//						__asm("NOP");
//					}
//
//	for (int i= 0; i<127; i++){
//		motorIncrementPwmLeft(-1);
//
//		for (int x = 0; x < 400000; x++) {
//					__asm("NOP");
//				}
//
//	}

// decoder test:
//	int encoder_speed_L[10] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
//
//	for (int i = 0; i <= 9; i++) {
//
//		//motorIncrementPwmLeft(12);
//
//		for (int x = 0; x < 4000000; x++) {
//			__asm("NOP");
//		}
//
//		encoder_speed_L[i] = quadv2GetSpeedLeft();
//
//		for (int y = 0; y < 4000000; y++) {
//			__asm("NOP");
//		}
//	}
//
//	for (int i = 0; i <= 9; i++) {
//
//		//motorIncrementPwmLeft(-12);
//
//		for (int z = 0; z < 4000000; z++) {
//			__asm("NOP");
//		}
//	}

// Never leave main
uint32_t uart = 0;
uint32_t uartOld = 0;
uint16_t receivedAngle, receivedSpeed;
uint32_t calculatedSpeed;
uint16_t quadLeft, quadRight;

	for (;;) {

		uart = uart0ReadInt32();
		if (uart != uartOld) {
			receivedSpeed = getspeed(uart);
			receivedAngle = getangle(uart);
			calculatedSpeed = toSpeed(uart0ReadInt32());
//			calculatedSpeed = toSpeed(0x0087000a


			printf( "______________________________________________________________________________|\r\n"
					"received values(%4d) |angle: %11d[degree] |speed: %13d       |\r\n"
					"calculated values     |speed left: %6d  [mm/s] |speed right: %7d[mm/s] |\r\n"
					"measured values       |encoder left: %4d  [mm/s] |encoder right: %5d[mm/s] |\r\n",
					uart0RxIntBufCount(), receivedAngle, receivedSpeed, (calculatedSpeed >> 16), (uint16_t)calculatedSpeed, quadv2GetSpeedLeft(), quadv2GetSpeedRight());
			uartOld = uart;
		}

	}
}

