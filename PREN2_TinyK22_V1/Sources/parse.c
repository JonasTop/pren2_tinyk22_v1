/*Parse.c
 * This file is meant to take the Hex Val. Sent from the raspberry pi and
 * decompose it. It contains functions to parse the Hex val int 4 different values.
 * 1Angle value and 2 speed values. The angle value will be translated into a speed
 * difference.
 * The output of these functions will be used in drive.c
 *
 *
 *  Created on: 21.02.2020
 *      Author: Marco Anderhalden, Riccardo Degiacomi, Jonas Tophinke
 */
#include "platform.h"
#include "stdlib.h"
#include "stdint.h"
#include "parse.h"
#include "uart.h"
#include "drive.h"

uint32_t oldspeed_angle;
uint16_t speedLeft, speedRight;
uint16_t speed;
uint16_t angle;
uint32_t calcSpeed;



//splits the value sent by the raspberry py
//returns only the speed value of the number
uint16_t getspeed(uint32_t num_in){
	uint32_t speed = num_in;
 return (uint16_t)speed;
}

//splits the value sent by the raspberry py
//returns only angle value of the number
uint16_t getangle(uint32_t num_in){
	uint32_t angle = num_in;
 return (uint16_t)(angle>>16);
}

uint32_t toSpeed(uint32_t speed_angle) {

	speed = getspeed(speed_angle) * 5; //PWM in percent from sent speed value  10 --> 40%
	angle = getangle(speed_angle);

	if (speed_angle != oldspeed_angle) {

		if (angle >= 0 & angle <= 90) {
			speedLeft = ((50 + 5 * angle / 9) * speed / 100); //(50 + (5 / 9 * angle)) = percentage of the reduction of the left wheel
			speedRight = speed;
		} else if (angle > 90 & angle <= 180) {
			speedRight = ((100 - 5 * (angle - 90) / 9) * speed / 100); //(100 + (5 / 9 * (angle - 90))) = percentage of the reduction of the right wheel
			speedLeft = speed;
		}

		motorSetPwmLeft(speedLeft);
		motorSetPwmRight(speedRight);

		speedLeft = (10 * speedLeft * 22.4 + 5) / 10;		//speed in mm/sek
		speedRight = (10 * speedRight * 22.4 + 5) / 10;		//speed in mm/sek
		oldspeed_angle = speed_angle;
	    calcSpeed = (((uint32_t) speedLeft) << 16) | (speedRight);

	}


	return calcSpeed;
}


uint8_t getSpeedLeft(void){
	return speedLeft*22.4;      //speed in mm/sek
}

uint8_t getSpeedRight(void){
	return speedRight*22.4;		//speed in mm/sek
}



uint8_t toDirection(uint8_t direction){
	return 0;
}


/**
 * Reads a null terminated string out of the rxBuf queue. The
 * function blocks until a new Line character has been received
 * or the length has been exceeded.
 *
 * @details
 *   the new line character will be replaced with a '\0' to
 *   terminate the string.
 *
 * @param[out] *str
 *   pointer to a char array to store the received string
 * @param[in] length
 *   the length of the str char array.
 * @returns
 *   the length of the received string.
 */
uint8_t getNum(uint8_t num_in){

uint8_t *num;
    if (uart1HasLineReceived())
    {
      uart1ReadLine(num, sizeof(num));
    }
    return 0;
}

